import Detector from './utils/detector';
import Main from './app/main';

import './../css/app.scss';


function init() {
  if (!Detector.webgl) {
    Detector.addGetWebGLMessage();
  } else {
    const container = document.getElementById('appContainer');

    new Main(container);
  }
}

init();
