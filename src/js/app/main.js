// Global imports -
import * as THREE from 'three';

import Controls from './components/controls';
import config from '../data/config';

export default class Main {
  constructor(container) {
    this.container = container;

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xffffff);
    this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 1000);

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.container.appendChild(this.renderer.domElement);

    this.controls = new Controls(this.camera, this.renderer.domElement);

    this.camera.position.set(0, 20, 10, 10);

    this.camera.position.z = 3;
    this.camera.position.x = 3;
    this.camera.position.y = 3;

    const geometry = new THREE.BoxGeometry(2, 8, 2);
    const loader = new THREE.TextureLoader();
    const material = new THREE.MeshLambertMaterial({
      map: loader.load('./assets/textures/testTexture.jpg'),
    });

    this.cube = new THREE.Mesh(geometry, material);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.6);
    directionalLight.position.set(3, 2, 1);

    const directionalLight2 = new THREE.DirectionalLight(0xffffff, 0.6);
    directionalLight2.position.set(-3, -2, -1);

    this.scene.add(directionalLight);
    this.scene.add(directionalLight2);

    this.scene.add(this.cube);

    const gui = new dat.GUI();
    const geometryFolder = gui.addFolder('Geometry');
    const geomentyWidthGui = geometryFolder.add({ width: config.width }, 'width', 0, 10).name('Cube width');
    const geomentyDepthGui = geometryFolder.add({ depth: config.depth }, 'depth', 0, 10).name('Cube depth');
    geomentyWidthGui.onChange((value) => {
      config.width = value;
    });
    geomentyDepthGui.onChange((value) => {
      config.depth = value;
    });

    const addButton = {
      add: () => {
        this.cube.scale.x = config.width;
        this.cube.scale.z = config.depth;
      }
    };

    geometryFolder.add(addButton, 'add').name("Set");

    this.render();
  }


  render() {
    requestAnimationFrame(this.render.bind(this));
    this.controls.threeControls.update();
    this.renderer.render(this.scene, this.camera);
  }
}
