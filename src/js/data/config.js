
export default {
  isDev: true,
  width: 2,
  depth: 4,
  controls: {
    autoRotate: false,
    autoRotateSpeed: -0.5,
    rotateSpeed: 0.5,
    zoomSpeed: 0.8,
    minDistance: 20,
    maxDistance: 400,
    minPolarAngle: 0,
    maxPolarAngle: Math.PI,
    minAzimuthAngle: -Math.PI,
    maxAzimuthAngle: Math.PI,
    enableDamping: true,
    dampingFactor: 0.5,
    enableZoom: true,
    target: {
      x: 0,
      y: 0,
      z: 0
    }
  },
  maxAnisotropy: 1,
  dpr: 1,
};
