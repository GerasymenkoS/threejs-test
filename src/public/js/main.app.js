(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/css/app.scss":
/*!**************************!*\
  !*** ./src/css/app.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/css/app.scss?");

/***/ }),

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils_detector__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/detector */ \"./src/js/utils/detector.js\");\n/* harmony import */ var _app_main__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app/main */ \"./src/js/app/main.js\");\n/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../css/app.scss */ \"./src/css/app.scss\");\n/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_app_scss__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\nfunction init() {\n  if (!_utils_detector__WEBPACK_IMPORTED_MODULE_0__[\"default\"].webgl) {\n    _utils_detector__WEBPACK_IMPORTED_MODULE_0__[\"default\"].addGetWebGLMessage();\n  } else {\n    var container = document.getElementById('appContainer');\n    new _app_main__WEBPACK_IMPORTED_MODULE_1__[\"default\"](container);\n  }\n}\n\ninit();\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/js/app/main.js":
/*!****************************!*\
  !*** ./src/js/app/main.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Main; });\n/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ \"./node_modules/three/build/three.module.js\");\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n// Global imports -\n\n\nvar Main =\n/*#__PURE__*/\nfunction () {\n  function Main(container) {\n    _classCallCheck(this, Main);\n\n    this.container = container;\n    this.scene = new three__WEBPACK_IMPORTED_MODULE_0__[\"Scene\"]();\n    this.scene.background = new three__WEBPACK_IMPORTED_MODULE_0__[\"Color\"](0xffffff);\n    this.camera = new three__WEBPACK_IMPORTED_MODULE_0__[\"PerspectiveCamera\"](45, window.innerWidth / window.innerHeight, 1, 10000);\n    this.renderer = new three__WEBPACK_IMPORTED_MODULE_0__[\"WebGLRenderer\"]();\n    this.renderer.setSize(window.innerWidth, window.innerHeight);\n    this.container.appendChild(this.renderer.domElement);\n    this.controls = new three__WEBPACK_IMPORTED_MODULE_0__[\"OrbitControls\"](this.camera, this.renderer.domElement);\n    var geometry = new three__WEBPACK_IMPORTED_MODULE_0__[\"BoxGeometry\"](1, 1, 1);\n    var material = new three__WEBPACK_IMPORTED_MODULE_0__[\"MeshBasicMaterial\"]({\n      color: 0x00ff00\n    });\n    var cube = new three__WEBPACK_IMPORTED_MODULE_0__[\"Mesh\"](geometry, material);\n    this.scene.add(cube);\n    this.camera.position.z = 5;\n    this.controls.update();\n    this.render();\n  }\n\n  _createClass(Main, [{\n    key: \"render\",\n    value: function render() {\n      requestAnimationFrame(this.render.bind(this));\n      this.controls.update();\n      this.renderer.render(this.scene, this.camera);\n    }\n  }]);\n\n  return Main;\n}();\n\n\n\n//# sourceURL=webpack:///./src/js/app/main.js?");

/***/ }),

/***/ "./src/js/utils/detector.js":
/*!**********************************!*\
  !*** ./src/js/utils/detector.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/**\n * @author alteredq / http://alteredqualia.com/\n * @author mr.doob / http://mrdoob.com/\n */\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  canvas: !!window.CanvasRenderingContext2D,\n  webgl: function () {\n    try {\n      var canvas = document.createElement('canvas');\n      return !!(window.WebGLRenderingContext && (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));\n    } catch (e) {\n      return false;\n    }\n  }(),\n  workers: !!window.Worker,\n  fileapi: window.File && window.FileReader && window.FileList && window.Blob,\n  getWebGLErrorMessage: function getWebGLErrorMessage() {\n    var element = document.createElement('div');\n    element.id = 'webgl-error-message';\n    element.style.fontFamily = 'monospace';\n    element.style.fontSize = '13px';\n    element.style.fontWeight = 'normal';\n    element.style.textAlign = 'center';\n    element.style.background = '#fff';\n    element.style.color = '#000';\n    element.style.padding = '1.5em';\n    element.style.width = '400px';\n    element.style.margin = '5em auto 0';\n\n    if (!this.webgl) {\n      element.innerHTML = window.WebGLRenderingContext ? ['Your graphics card does not seem to support <a href=\"http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation\" style=\"color:#000000\">WebGL</a>.<br />', 'Find out how to get it <a href=\"http://get.webgl.org/\" style=\"color:#000000\">here</a>.'].join('\\n') : ['Your browser does not seem to support <a href=\"http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation\" style=\"color:#000000\">WebGL</a>.<br/>', 'Find out how to get it <a href=\"http://get.webgl.org/\" style=\"color:#000000\">here</a>.'].join('\\n');\n    }\n\n    return element;\n  },\n  addGetWebGLMessage: function addGetWebGLMessage(parameters) {\n    var parent, id, element;\n    parameters = parameters || {};\n    parent = parameters.parent !== undefined ? parameters.parent : document.body;\n    id = parameters.id !== undefined ? parameters.id : 'oldie';\n    element = this.getWebGLErrorMessage();\n    element.id = id;\n    parent.appendChild(element);\n  }\n});\n\n//# sourceURL=webpack:///./src/js/utils/detector.js?");

/***/ }),

/***/ 0:
/*!*****************************!*\
  !*** multi ./src/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! ./src/js/app.js */\"./src/js/app.js\");\n\n\n//# sourceURL=webpack:///multi_./src/js/app.js?");

/***/ })

},[[0,"runtime","vendors"]]]);